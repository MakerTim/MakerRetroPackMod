package nl.makertim.mrp;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.google.common.collect.BiMap;

import net.minecraft.client.util.RecipeBookClient;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.RegistryNamespaced;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.registries.ForgeRegistry;

public class RecipeHelper {

	public static void registerOreDic(String name, ItemStack... items) {
		for (ItemStack item : items) {
			OreDictionary.registerOre(name, item);
		}
	}

	@SuppressWarnings("unchecked")
	public static void removeOreDic(String name, ItemStack... items) { // TODO
		int id = OreDictionary.getOreID(name);
		try {
			Class<?> oreDicClass = OreDictionary.class;
			Field idToStackUnField = oreDicClass.getDeclaredFields()[4];
			idToStackUnField.setAccessible(true);
			List<NonNullList<ItemStack>> idToStackUn = (List) idToStackUnField.get(null);
			idToStackUnField.setAccessible(false);
			NonNullList<ItemStack> oreDic = idToStackUn.get(id);

			for (ItemStack toRemove : items) {
				oreDic.removeIf(toRemove::isItemEqual);
			}
			idToStackUn.remove(id);
			idToStackUn.add(id, oreDic);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static FluidStack getFluidStack(String name, int amount) {
		Fluid fluid = FluidRegistry.getFluid(name);
		if (fluid == null) {
			return new FluidStack(FluidRegistry.LAVA, amount);
		}
		return new FluidStack(fluid, amount);
	}

	public static ItemStack getItemStack(String resourceName) {
		int index = resourceName.indexOf(":");
		if (index == -1) {
			return ItemStack.EMPTY;
		}
		String group = resourceName.substring(0, index);
		String name = resourceName.substring(index + 1);
		short data = 0;
		int times = 1;
		index = name.indexOf(":");
		if (index > 0) {
			String oldName = name;
			name = oldName.substring(0, index);
			oldName = oldName.substring(index + 1);
			index = oldName.indexOf("*");
			if (index > 0) {
				String timesString = oldName.substring(index + 1);
				oldName = oldName.substring(0, index);
				times = Integer.parseInt(timesString);
			}
			data = Short.parseShort(oldName);
		} else {
			index = name.indexOf("*");
			if (index > 0) {
				String timesString = name.substring(index + 1);
				name = name.substring(0, index);
				times = Integer.parseInt(timesString);
			}
		}
		return getItemStack(group, name, times, data);
	}

	public static ItemStack getItemStack(String domain, String name) {
		return getItemStack(domain, name, 1, (short) 0);
	}

	public static ItemStack getItemStack(String domain, String name, int amount) {
		return getItemStack(domain, name, amount, (short) 0);
	}

	public static ItemStack getItemStack(String domain, String name, short dmg) {
		return getItemStack(domain, name, 1, dmg);
	}

	public static ItemStack getItemStack(String domain, String name, int amount, short dmg) {
		Item item = Item.REGISTRY.getObject(new ResourceLocation(domain, name));
		if (item == null) {
			System.out.println(String.format("Item MISSING IS => %s:%s\n", domain, name));
			return null;
		}
		return new ItemStack(item, amount, dmg);
	}

	public static void registerShapelessRecipe(ResourceLocation name, ResourceLocation group, ItemStack output, Ingredient... components) {
		try {
			if (output == null) {
				output = ItemStack.EMPTY;
			}
			GameRegistry.addShapelessRecipe(name, group, output, components);
		} catch (Exception ex) {
			System.err.println(ex);
		}
	}

	public static Ingredient of(ItemStack is) {
		if (is == null || is.isEmpty()) {
			return null;
		}
		return Ingredient.fromStacks(is);
	}

	@SafeVarargs
	public static void registerShapedRecipe(ResourceLocation name, ResourceLocation group, ItemStack output, String row1, String row2, String row3, Map.Entry<Character, Ingredient>... components) {
		if (output == null) {
			output = ItemStack.EMPTY;
		}
		Object[] recipeStuff = new Object[(components.length * 2) + 3];
		int i = 0;
		recipeStuff[i++] = row1;
		recipeStuff[i++] = row2;
		recipeStuff[i++] = row3;
		for (Map.Entry component : components) {
			recipeStuff[i++] = component.getKey();
			recipeStuff[i++] = component.getValue();
		}
		try {
			GameRegistry.addShapedRecipe(name, group, output, recipeStuff);
		} catch (Exception ex) {
			System.err.println(ex);
		}
	}

	@SuppressWarnings("unchecked")
	public static void removeRecipes(List<ItemStack> resultItems) {
		RegistryNamespaced<ResourceLocation, IRecipe> registry = CraftingManager.REGISTRY;
		try {
			ForgeRegistry<IRecipe> delegate = getReflectionFrom(registry.getClass(), registry, 1, ForgeRegistry.class);
			BiMap<Integer, IRecipe> ids = getReflectionFrom(delegate.getClass(), delegate, 2, BiMap.class);
			BiMap<ResourceLocation, IRecipe> names = getReflectionFrom(delegate.getClass(), delegate, 3, BiMap.class);
			Optional<RecipeListWrapper> wrapper;
			if (FMLCommonHandler.instance().getEffectiveSide().isClient()) {
				wrapper = Optional.of(new RecipeListWrapper(RecipeBookClient.ALL_RECIPES));
			} else {
				wrapper = Optional.empty();
			}

			resultItems.forEach(output -> {
				wrapper.ifPresent(wrapped -> wrapped.removeIf(recipe -> recipe.getRecipeOutput().isItemEqual(output)));
				names.entrySet().removeIf(nameRecipe -> {
					if (nameRecipe.getValue().getRecipeOutput().isItemEqual(output)) {
						IRecipe recipe = nameRecipe.getValue();
						int id = delegate.getID(recipe);
						ids.remove(id);
						return true;
					}
					return false;
				});
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static void removeFurnaceRecipe(List<ItemStack> resultItems) {
		FurnaceRecipes smeltingBase = FurnaceRecipes.instance();
		try {
			Map<ItemStack, ItemStack> smeltingList = getReflectionFrom(smeltingBase.getClass(), smeltingBase, 1,
				Map.class);
			Map<ItemStack, Float> experienceList = getReflectionFrom(smeltingBase.getClass(), smeltingBase, 2,
				Map.class);
			int i = 0;

			resultItems.forEach(toRemove -> //
			smeltingList.entrySet().removeIf(ii -> ii.getKey().isItemEqual(toRemove)));
			resultItems.forEach(toRemove -> //
			experienceList.entrySet().removeIf(ii -> ii.getKey().isItemEqual(toRemove) ));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	static void intChangeValue(String className, Object of, String valueName, double multi) {
		try {
			Class<?> clazz = Class.forName(className);
			Field field = clazz.getField(valueName);
			field.set(null, (int) ((Integer) field.get(of) * multi));
		} catch (Exception ex) {
			System.err.println("### ERRNOR- " + className + ":" + valueName + " * " + multi);
			ex.printStackTrace();
		}
	}

	static void doubleChangeValue(String className, Object of, String valueName, double multi) {
		try {
			Class<?> clazz = Class.forName(className);
			Field field = clazz.getField(valueName);
			field.set(null, ((Double) field.get(of) * multi));
		} catch (Exception ex) {
			System.err.println("### ERRNOR- " + className + ":" + valueName + " * " + multi);
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private static <T> T getReflectionFrom(Class from, Object object, int fieldNumber, Class<T> castTo) {
		Field field = from.getDeclaredFields()[fieldNumber];
		boolean isAccessible = field.isAccessible();
		field.setAccessible(true);
		T ret = null;
		try {
			ret = (T) field.get(object);
		} catch (IllegalAccessException ex) {
			throw new RuntimeException(ex);
		}
		field.setAccessible(isAccessible);
		return ret;
	}
}
