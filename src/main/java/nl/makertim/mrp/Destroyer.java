package nl.makertim.mrp;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

/**
 * @author Tim Biesenbeek
 */
public class Destroyer {
	public static Optional<ThermalExpansionWrapper> thermalExpansionWrapper = Optional.empty();
	public static Optional<BuildcraftWrapper> bcWrapper = Optional.empty();

	public static List<ItemStack> toRemove = new NoNullList<>(Arrays.asList( //
		from("calculator:storagemodule"), //
		from("calculator:warpmodule"), //
		from("calculator:jumpmodule"), //
		from("calculator:hungermodule"), //
		from("calculator:healthmodule"), //
		from("calculator:nutritionmodule"), //
		from("calculator:terrainmodule"), //
		from("calculator:advancedterrainmodule"), //
		from("calculator:locatormodule"), //
		from("draconicevolution:infused_obsidian"), //
		from("draconicevolution:dislocator"), //
		from("draconicevolution:dislocator_advanced"), //
		from("draconicevolution:dislocator_pedestal"), //
		from("draconicevolution:dislocator_receptacle"), //
		from("extrautils2:cursedearth"), //
		from("extrautils2:ingredients:10"), //
		from("extrautils2:ingredients:13"), //
		from("extrautils2:powerbattery"), //
		from("extrautils2:powertransmitter"), //
		from("extrautils2:grocket:0"), //
		from("extrautils2:grocket:1"), //
		from("extrautils2:grocket:2"), //
		from("extrautils2:grocket:3"), //
		from("extrautils2:grocket:4"), //
		from("extrautils2:grocket:5"), //
		from("extrautils2:pipe"), //
		from("extrautils2:bagofholding"), //
		from("extrautils2:flattransfernode:0"), //
		from("extrautils2:flattransfernode:1"), //
		from("rftools:builder"), //
		from("rftools:elevator"), //
		from("rftools:block_protector"), //
		from("rftools:item_filter"), //
		from("rftools:security_manager"), //
		from("rftools:shield_template_block"), //
		from("rftools:shield_block1"), //
		from("rftools:shield_block2"), //
		from("rftools:shield_block3"), //
		from("rftools:shield_block4"), //
		from("rftools:matter_transmitter"), //
		from("rftools:matter_receiver"), //
		from("rftools:dialing_device"), //
		from("rftools:destination_analyzer"), //
		from("rftools:powercell"), //
		from("rftools:powercell_card"), //
		from("rftools:powercell_advanced"), //
		from("rftools:powercell_simple"), //
		from("rftools:spawner"), //
		from("rftools:remote_storage"), //
		from("rftools:remote_scanner"), //
		from("rftools:charged_porter"), //
		from("rftools:advanced_charged_porter"), //
		from("rftools:space_chamber"), //
		from("rftools:space_chamber_controller"), //
		from("rftools:projector"), //
		from("storagedrawers:controller"), //
		from("storagedrawers:controllerslave"), //
		from("thermaldynamics:duct_16:1"), //
		from("thermaldynamics:duct_16:3"), //
		from("thermaldynamics:duct_16:5"), //
		from("thermaldynamics:duct_16:7"), //
		from("thermaldynamics:duct_32:1"), //
		from("thermaldynamics:duct_32:3"), //
		from("thermaldynamics:duct_32:5"), //
		from("thermaldynamics:duct_32:7"), //
		from("thermaldynamics:retriever:0"), //
		from("thermaldynamics:retriever:1"), //
		from("thermaldynamics:retriever:2"), //
		from("thermaldynamics:retriever:3"), //
		from("thermaldynamics:retriever:4"), //
		from("thermaldynamics:filter:0"), //
		from("thermaldynamics:filter:1"), //
		from("thermaldynamics:filter:2"), //
		from("thermaldynamics:filter:3"), //
		from("thermaldynamics:filter:4"), //
		from("calculator:advancedpowercube") //
	));
	public static List<ItemStack> toDisableSmelting = new NoNullList<>(Arrays.asList( //
		from("draconicevolution:draconium_dust"), //
		from("draconicevolution:draconium_ingot"), //
		from("draconicevolution:draconium_ore"), //
		from("draconicevolution:draconium_ore:1"), //
		from("draconicevolution:draconium_ore:2"), //
		from("draconicevolution:draconium_dust:32767") //
	));
	public static List<ItemStack> toReplace = new NoNullList<>(Arrays.asList( //
		from("calculator:atomicmultiplier"), //
		from("draconicevolution:draconium_ingot"), //
		from("computercraft:computer"), //
		from("computercraft:computer:16384"), //
		from("enderstorage:ender_storage"), //
		from("enderstorage:ender_storage:1"), //
		from("projecte:condenser_mk1"), //
		from("projecte:item.pe_philosophers_stone"), //
		from("projecte:transmutation_table"), //
		from("extrautils2:klein"), //
		from("extrautils2:teleporter:1")) //
	);

	public static ItemStack from(String name) {
		return RecipeHelper.getItemStack(name);
	}

	public static void removeRecipes() {
		RecipeHelper.removeRecipes(toRemove);
		RecipeHelper.removeRecipes(toReplace);
	}

	public static void removeFurnace() {
		RecipeHelper.removeFurnaceRecipe(toDisableSmelting);
	}

	public static void addRecipes() {
		RecipeHelper.registerShapedRecipe( //
			new ResourceLocation(MRPMod.MODID, "cccomputer2"), null, //
			from("computercraft:computer:16384"), //
			"ggg", //
			"grg", //
			"GpG", //
			new AbstractMap.SimpleEntry<>('p', RecipeHelper.of(from("minecraft:stained_glass_pane:8"))),
			new AbstractMap.SimpleEntry<>('r', RecipeHelper.of(from("calculator:calculator"))),
			new AbstractMap.SimpleEntry<>('g', RecipeHelper.of(from("minecraft:gold_ingot"))),
			new AbstractMap.SimpleEntry<>('G', RecipeHelper.of(from("forestry:thermionic_tubes:4"))));
		RecipeHelper.registerShapedRecipe( //
			new ResourceLocation(MRPMod.MODID, "atomicmultiplier"), null, //
			from("calculator:atomicmultiplier"), //
			"PCP", //
			"MAM", //
			"IEI", //
			new AbstractMap.SimpleEntry<>('P', RecipeHelper.of(from("calculator:calculatorplug"))),
			new AbstractMap.SimpleEntry<>('C', RecipeHelper.of(from("calculator:fabricationchamber"))),
			new AbstractMap.SimpleEntry<>('M', RecipeHelper.of(from("calculator:atomicmodule"))),
			new AbstractMap.SimpleEntry<>('A', RecipeHelper.of(from("calculator:atomicassembly"))),
			new AbstractMap.SimpleEntry<>('I', RecipeHelper.of(from("ic2:crafting:4"))),
			new AbstractMap.SimpleEntry<>('E', RecipeHelper.of(from("calculator:enddiamond"))));
		RecipeHelper.registerShapedRecipe( //
			new ResourceLocation(MRPMod.MODID, "computer1"), null, //
			from("computercraft:computer"), //
			"SSS", //
			"SCS", //
			"SGS", //
			new AbstractMap.SimpleEntry<>('S', RecipeHelper.of(from("minecraft:stone"))),
			new AbstractMap.SimpleEntry<>('C', RecipeHelper.of(from("calculator:infocalculator"))),
			new AbstractMap.SimpleEntry<>('G', RecipeHelper.of(from("minecraft:glass_pane"))));
		RecipeHelper.registerShapedRecipe( //
			new ResourceLocation(MRPMod.MODID, "computer2"), null, //
			from("computercraft:computer:16384"), //
			"SSS", //
			"SCS", //
			"SGS", //
			new AbstractMap.SimpleEntry<>('S', RecipeHelper.of(from("minecraft:gold"))),
			new AbstractMap.SimpleEntry<>('C', RecipeHelper.of(from("calculator:infocalculator"))),
			new AbstractMap.SimpleEntry<>('G', RecipeHelper.of(from("minecraft:glass_pane"))));
		RecipeHelper.registerShapedRecipe( //
			new ResourceLocation(MRPMod.MODID, "trans"), null, //
			from("projecte:transmutation_table"), //
			"DSD", //
			"S-S", //
			"DSD", //
			new AbstractMap.SimpleEntry<>('D', RecipeHelper.of(from("projecte:item.pe_matter"))),
			new AbstractMap.SimpleEntry<>('S', RecipeHelper.of(from("ic2:misc_resource:1"))),
			new AbstractMap.SimpleEntry<>('-', RecipeHelper.of(from("projecte:item.pe_philosophers_stone"))));
		RecipeHelper.registerShapedRecipe( //
			new ResourceLocation(MRPMod.MODID, "condenser"), null, //
			from("projecte:condenser_mk1"), //
			"SOS", //
			"OCO", //
			"S-S", //
			new AbstractMap.SimpleEntry<>('S', RecipeHelper.of(from("extrautils2:simpledecorative:2"))),
			new AbstractMap.SimpleEntry<>('O', RecipeHelper.of(from("draconicevolution:draconium_dust"))),
			new AbstractMap.SimpleEntry<>('C', RecipeHelper.of(from("projecte:alchemical_chest"))),
			new AbstractMap.SimpleEntry<>('-', RecipeHelper.of(from("projecte:item.pe_philosophers_stone"))));
		RecipeHelper.registerShapedRecipe( //
			new ResourceLocation(MRPMod.MODID, "pestone"), null, //
			from("projecte:item.pe_philosophers_stone"), //
			"RGR", //
			"GSG", //
			"RDR", //
			new AbstractMap.SimpleEntry<>('R', RecipeHelper.of(from("minecraft:redstone"))),
			new AbstractMap.SimpleEntry<>('G', RecipeHelper.of(from("minecraft:glowstone_dust"))),
			new AbstractMap.SimpleEntry<>('D', RecipeHelper.of(from("minecraft:diamond"))),
			new AbstractMap.SimpleEntry<>('S', RecipeHelper.of(from("mrp:infinitysoul"))));
		RecipeHelper.registerShapedRecipe( //
			new ResourceLocation(MRPMod.MODID, "kleinflask"), null, //
			from("extrautils2:klein"), //
			"TTT", //
			"T T", //
			"TTT", //
			new AbstractMap.SimpleEntry<>('T', RecipeHelper.of(from("ic2:crafting:10"))));
		RecipeHelper.registerShapedRecipe( //
			new ResourceLocation(MRPMod.MODID, "extrateleporter"), null, //
			from("extrautils2:teleporter:1"), //
			"TTT", //
			"T T", //
			"TTT", //
			new AbstractMap.SimpleEntry<>('T', RecipeHelper.of(from("extrautils2:compressedcobblestone:7"))));
		RecipeHelper.registerShapedRecipe( //
			new ResourceLocation(MRPMod.MODID, "draconium_1"), null, //
			from("draconicevolution:draconium_ingot"), //
			"nnn", //
			"nnn", //
			"nnn", //
			new AbstractMap.SimpleEntry<>('n', RecipeHelper.of(from("draconicevolution:nugget:0"))));
		RecipeHelper.registerShapelessRecipe( //
			new ResourceLocation(MRPMod.MODID, "draconium_2"), null, //
			from("draconicevolution:draconium_ingot*9"), //
			RecipeHelper.of(from("draconicevolution:draconium_block")));
		thermalExpansionWrapper.ifPresent(ThermalExpansionWrapper::registerRecipes);
		bcWrapper.ifPresent(BuildcraftWrapper::registerRecipes);
	}

	public static void creativeAge() {
		ItemStack creative = new ItemStack(MRPMod.items[1]);
		{
			RecipeHelper.registerShapelessRecipe( //
				new ResourceLocation(MRPMod.MODID, "C0"), null, //
				creative, //
				RecipeHelper.of(from("projecte:item.pe_philosophers_stone")), //
				RecipeHelper.of(from("extrabees:honey_comb:66")), //
				RecipeHelper.of(from("tconstruct:slime:2")), //
				RecipeHelper.of(from("minecraft:purple_glazed_terracotta")), //
				RecipeHelper.of(from("gendustry:honey_comb:15")), //
				RecipeHelper.of(from("ironchest:iron_shulker_box_purple:4")), //
				RecipeHelper.of(from("extratrees:fences.1:13")), //
				RecipeHelper.of(from("tconstruct:slime_sapling:1")), //
				RecipeHelper.of(from("sonarcore:stablestoneblackrimmed_purple")));
		}

		ItemStack engine = from("buildcraftcore:engine:3");
		{
			RecipeHelper.registerShapedRecipe( //
				new ResourceLocation(MRPMod.MODID, "C1"), null, //
				engine, //
				"AAA", //
				"ABA", //
				"AAA", //
				new AbstractMap.SimpleEntry<>('A', RecipeHelper.of(from("buildcraftcore:engine:2"))),
				new AbstractMap.SimpleEntry<>('B', RecipeHelper.of(creative)));
		}
		ItemStack spike = from("extrautils2:spike_creative");
		{
			RecipeHelper.registerShapedRecipe( //
				new ResourceLocation(MRPMod.MODID, "C2"), null, //
				spike, //
				" A ", //
				"ABA", //
				"BCB", //
				new AbstractMap.SimpleEntry<>('A', RecipeHelper.of(from("extrautils2:lawsword"))),
				new AbstractMap.SimpleEntry<>('C', RecipeHelper.of(engine)),
				new AbstractMap.SimpleEntry<>('B', RecipeHelper.of(from("calculator:endforgedsword"))));
		}
		ItemStack pool = from("botania:pool:1");
		{
			RecipeHelper.registerShapedRecipe( //
				new ResourceLocation(MRPMod.MODID, "C3"), null, //
				pool, //
				"   ", //
				"ABA", //
				"AAA", //
				new AbstractMap.SimpleEntry<>('A', RecipeHelper.of(from("botania:manaresource:14"))),
				new AbstractMap.SimpleEntry<>('B', RecipeHelper.of(spike)));
		}
		ItemStack grid = from("extrautils2:passivegenerator:6");
		{
			RecipeHelper.registerShapedRecipe( //
				new ResourceLocation(MRPMod.MODID, "C4"), null, //
				grid, //
				"AAA", //
				"ACA", //
				"ABA", //
				new AbstractMap.SimpleEntry<>('A', RecipeHelper.of(from("extrautils2:decorativesolid:3"))),
				new AbstractMap.SimpleEntry<>('C', RecipeHelper.of(from("extrautils2:ingredients:0"))),
				new AbstractMap.SimpleEntry<>('B', RecipeHelper.of(pool)));
		}
		ItemStack itemRF = from("draconicevolution:draconium_capacitor:2");
		{
			RecipeHelper.registerShapedRecipe( //
				new ResourceLocation(MRPMod.MODID, "C5"), null, //
				itemRF, //
				"ABA", //
				"BCB", //
				"ABA", //
				new AbstractMap.SimpleEntry<>('A', RecipeHelper.of(from("draconicevolution:draconic_block"))),
				new AbstractMap.SimpleEntry<>('B', RecipeHelper.of(from("draconicevolution:chaotic_core"))),
				new AbstractMap.SimpleEntry<>('C', RecipeHelper.of(grid)));
		}
		ItemStack eu = from("ic2:te:86");
		{
			RecipeHelper.registerShapedRecipe( //
				new ResourceLocation(MRPMod.MODID, "C6"), null, //
				eu, //
				"SAS", //
				"BBB", //
				"SCS", //
				new AbstractMap.SimpleEntry<>('C', RecipeHelper.of(from("minecraft:furnace"))),
				new AbstractMap.SimpleEntry<>('B', RecipeHelper.of(from("ic2:nuclear:10"))),
				new AbstractMap.SimpleEntry<>('A', RecipeHelper.of(itemRF)),
				new AbstractMap.SimpleEntry<>('S', RecipeHelper.of(from("compactsolars:compact_solar_block:2"))));
		}
		ItemStack fabrique = from("thermalfoundation:upgrade:256");
		{
			RecipeHelper.registerShapelessRecipe( //
				new ResourceLocation(MRPMod.MODID, "C7"), null, fabrique, //
				RecipeHelper.of(eu), RecipeHelper.of(from("thermalfoundation:upgrade:35")));
		}
		ItemStack rf1 = from("draconicevolution:creative_rf_source");
		{
			RecipeHelper.registerShapedRecipe( //
				new ResourceLocation(MRPMod.MODID, "C8"), null, //
				rf1, //
				"AAA", //
				"ABA", //
				"AAA", //
				new AbstractMap.SimpleEntry<>('A', RecipeHelper.of(from("draconicevolution:draconium_chest"))),
				new AbstractMap.SimpleEntry<>('B', RecipeHelper.of(fabrique)));
		}
		ItemStack rf2 = from("calculator:creativepowercube");
		{
			RecipeHelper.registerShapedRecipe( //
				new ResourceLocation(MRPMod.MODID, "C9"), null, //
				rf2, //
				"AAA", //
				"ABA", //
				"AAA", //
				new AbstractMap.SimpleEntry<>('A', RecipeHelper.of(from("calculator:material:9"))),
				new AbstractMap.SimpleEntry<>('B', RecipeHelper.of(rf1)));
		}
		ItemStack liquid = from("extrautils2:drum:4");
		{
			RecipeHelper.registerShapedRecipe( //
				new ResourceLocation(MRPMod.MODID, "C10"), null, //
				liquid, //
				"ACA", //
				"CBC", //
				"ACA", //
				new AbstractMap.SimpleEntry<>('C', RecipeHelper.of(from("extrautils2:ingredients:17"))),
				new AbstractMap.SimpleEntry<>('B', RecipeHelper.of(from("extrautils2:teleporter:1"))),
				new AbstractMap.SimpleEntry<>('A', RecipeHelper.of(rf2)));
		}
		ItemStack replace = from("draconicevolution:creative_exchanger");
		{
			RecipeHelper.registerShapedRecipe( //
				new ResourceLocation(MRPMod.MODID, "C11"), null, //
				replace, //
				"   ", //
				"B B", //
				"AAA", //
				new AbstractMap.SimpleEntry<>('B', RecipeHelper.of(rf2)),
				new AbstractMap.SimpleEntry<>('A', RecipeHelper.of(liquid)));
		}
		ItemStack backpack = from("thermalexpansion:satchel:32000");
		ItemStack storage = from("storagedrawers:upgrade_creative:1");
		{
			RecipeHelper.registerShapelessRecipe( //
				new ResourceLocation(MRPMod.MODID, "C12"), null, replace, //
				RecipeHelper.of(backpack));
			RecipeHelper.registerShapelessRecipe( //
				new ResourceLocation(MRPMod.MODID, "C13"), null, backpack, //
				RecipeHelper.of(storage));
			RecipeHelper.registerShapelessRecipe( //
				new ResourceLocation(MRPMod.MODID, "C14"), null, storage, //
				RecipeHelper.of(replace));
		}
	}

	public static void removeFrom(JEIBlacklistWrapper blacklist) {
		toRemove.forEach(blacklist::addIngredientToBlacklist);
	}

}
