package nl.makertim.mrp;

import java.util.Arrays;
import java.util.Optional;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistry;

@Mod(modid = MRPMod.MODID, version = MRPMod.VERSION, dependencies = "after:jei;after:draconicevolution;after:calculator;after:enderstorage;after:extrautils2;after:thermaldynamics;after:rftools;after:storagedrawers")
@Mod.EventBusSubscriber(modid = MRPMod.MODID)
public class MRPMod {
	public static final String MODID = "mrp";
	public static final String VERSION = "1.0";

	public static CraftingItem[] items;

	@EventHandler
	public void pre(FMLPreInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(this);
	}

	@EventHandler
	public void post(FMLPostInitializationEvent event) {
		try {
			Class.forName("cofh.thermalexpansion.util.managers.machine.SmelterManager");
			Destroyer.thermalExpansionWrapper = Optional.of(new ThermalExpansionWrapper());
		} catch (ClassNotFoundException ignore) {
			System.err.println("No Thermal Expansion Found!");
		}
		try {
			Class.forName("buildcraft.core.BCCore");
			Destroyer.bcWrapper = Optional.of(new BuildcraftWrapper());
		} catch (ClassNotFoundException ignore) {
			System.err.println("No Thermal Expansion Found!");
		}

		Destroyer.removeRecipes();
		Destroyer.addRecipes();
		Destroyer.creativeAge();
	}

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {
		items = new CraftingItem[]{ //
				new CraftingItem("infinitysoul"), //
				new CraftingItem("creative"), //
		};

		IForgeRegistry<Item> registery = event.getRegistry();
		Arrays.stream(items).forEach(registery::register);
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public static void registerModel(ModelRegistryEvent event) {
		Arrays.stream(items).forEach(item -> {
			ModelLoader.setCustomModelResourceLocation(item, 0,
				new ModelResourceLocation(new ResourceLocation(MODID, item.itemName), null));
		});
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void registerEvent(RegistryEvent.Register<IRecipe> event) {
		Destroyer.removeFurnace();
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void registerEvent(BlockEvent.BreakEvent event) {
		if (!event.isCanceled()) {
			Block block = event.getState().getBlock();
			if (block.getUnlocalizedName().equals("tile.mobSpawner")) {
				BlockPos pos = event.getPos();
				EntityItem item = new EntityItem(event.getWorld(), pos.getX(), pos.getY(), pos.getZ(),
						new ItemStack(items[0], 1));
				event.getWorld().spawnEntity(item);
			}
		}
	}

}
