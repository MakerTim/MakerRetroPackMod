package nl.makertim.mrp;

import net.minecraft.client.gui.recipebook.RecipeList;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * @author Tim Biesenbeek
 */
public class RecipeListWrapper {

	@SideOnly(Side.CLIENT)
	private List<net.minecraft.client.gui.recipebook.RecipeList> wrapper;

	public RecipeListWrapper(List<RecipeList> allRecipes) {
		wrapper = allRecipes;
	}

	public void removeIf(Predicate<IRecipe> filter) {
		wrapper.removeIf(list -> {
			list.getRecipes().removeIf(filter);
			return list.getRecipes().isEmpty();
		});
	}

	public void forEach(Consumer<IRecipe> recipe) {
		wrapper.forEach(list -> list.getRecipes().forEach(recipe));
	}

}
