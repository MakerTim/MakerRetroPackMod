package nl.makertim.mrp;

public class JEIBlacklistWrapper {

	private mezz.jei.api.ingredients.IIngredientBlacklist blacklist;

	public JEIBlacklistWrapper(mezz.jei.api.ingredients.IIngredientBlacklist blacklist) {
		this.blacklist = blacklist;
	}

	public <V> void addIngredientToBlacklist(V var1) {
		try {
			blacklist.addIngredientToBlacklist(var1);
		} catch (Exception ex) {
			System.err.println(ex);
		}
	}

	public <V> void removeIngredientFromBlacklist(V var1) {
		blacklist.removeIngredientFromBlacklist(var1);
	}

	public <V> boolean isIngredientBlacklisted(V var1) {
		return blacklist.isIngredientBlacklisted(var1);
	}
}