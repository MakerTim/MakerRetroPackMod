package nl.makertim.mrp;

import mezz.jei.api.IModPlugin;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.JEIPlugin;
import mezz.jei.api.ingredients.IIngredientBlacklist;

/**
 * @author Tim Biesenbeek
 */
@JEIPlugin
public class JEIRemoverPlugin implements IModPlugin {

	@Override
	public void register(IModRegistry registry) {
		IIngredientBlacklist blacklist = registry.getJeiHelpers().getIngredientBlacklist();
		JEIBlacklistWrapper wrapper = new JEIBlacklistWrapper(blacklist);
		Destroyer.removeFrom(wrapper);
	}

}
