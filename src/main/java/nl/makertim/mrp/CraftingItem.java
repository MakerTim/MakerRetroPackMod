package nl.makertim.mrp;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 * @author Tim Biesenbeek
 */
public class CraftingItem extends Item {

	public String itemName;

	public CraftingItem(final String itemName) {
		this.itemName = itemName;
		setRegistryName(MRPMod.MODID, itemName);
		setUnlocalizedName(getRegistryName().toString());
		setCreativeTab(CreativeTabs.MISC);
	}

}