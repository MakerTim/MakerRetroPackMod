package nl.makertim.mrp;

import buildcraft.api.fuels.BuildcraftFuelRegistry;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;

/**
 * @author Tim Biesenbeek
 */
public class BuildcraftWrapper {

	public void registerRecipes() {
        BuildcraftFuelRegistry.fuel.addFuel(new FluidStack(FluidRegistry.getFluid("bio.ethanol"), 1), 5000000, 25000);
        BuildcraftFuelRegistry.fuel.addFuel(new FluidStack(FluidRegistry.getFluid("biomass"), 1), 1000000, 5000);
	}
}
