package nl.makertim.mrp;

import net.minecraft.util.NonNullList;

import java.util.Collection;

/**
 * @author Tim Biesenbeek
 */
public class NoNullList<E> extends NonNullList<E> {

	public NoNullList(Collection<E> collection) {
		super();
		this.addAll(collection);
	}
	
	public NoNullList() {
		super();
	}

	@Override
	public boolean add(E e) {
		return e != null && super.add(e);
	}
}
