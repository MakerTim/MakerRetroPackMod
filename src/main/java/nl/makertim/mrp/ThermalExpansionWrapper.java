package nl.makertim.mrp;

import cofh.thermalexpansion.util.managers.machine.SmelterManager;
import cofh.thermalexpansion.util.managers.machine.TransposerManager;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidStack;

/**
 * @author Tim Biesenbeek
 */
public class ThermalExpansionWrapper {

	public void registerRecipes() {
		addAlloy(100000, RecipeHelper.getItemStack("draconicevolution:draconium_dust*4"),
			RecipeHelper.getItemStack("ic2:crafting:4"), RecipeHelper.getItemStack("draconicevolution:draconium_ingot"),
			RecipeHelper.getItemStack("ic2:misc_resource:2"), 15);
		addAlloy(100000, RecipeHelper.getItemStack("ic2:crafting:4"), RecipeHelper.getItemStack("ic2:misc_resource:2"),
			RecipeHelper.getItemStack("ic2:misc_resource:2*44"),
			RecipeHelper.getItemStack("draconicevolution:draconium_dust"), 5);

		addAlloy(1000, RecipeHelper.getItemStack("ic2:nuclear:7*3"),
			RecipeHelper.getItemStack("thermalfoundation:material:135"),
			RecipeHelper.getItemStack("thermalfoundation:material:134"), ItemStack.EMPTY, 0);
		addAlloy(1000, RecipeHelper.getItemStack("ic2:misc_resource:1"),
				RecipeHelper.getItemStack("minecraft:stone"),
				RecipeHelper.getItemStack("thermalfoundation:ore:7"), ItemStack.EMPTY, 0);

		addFluid(5000, RecipeHelper.getItemStack("minecraft:chest*2"),
			RecipeHelper.getItemStack("enderstorage:ender_storage"), RecipeHelper.getFluidStack("enderium", 144 * 8));
		addFluid(5000, RecipeHelper.getItemStack("minecraft:shulker_box"),
				RecipeHelper.getItemStack("enderstorage:ender_storage"), RecipeHelper.getFluidStack("enderium", 144));
		addFluid(5000, RecipeHelper.getItemStack("minecraft:cauldron*2"),
			RecipeHelper.getItemStack("enderstorage:ender_storage:1"), RecipeHelper.getFluidStack("enderium", 144 * 8));

		removeAlloy(RecipeHelper.getItemStack("draconicevolution:draconium_ore"), RecipeHelper.getItemStack("thermalfoundation:material:865"));
		removeAlloy(RecipeHelper.getItemStack("draconicevolution:draconium_ore"), RecipeHelper.getItemStack("thermalfoundation:material:866"));
		removeAlloy(RecipeHelper.getItemStack("draconicevolution:draconium_dust"), RecipeHelper.getItemStack("minecraft:sand"));
		removeAlloy(RecipeHelper.getItemStack("draconicevolution:draconium_dust"), RecipeHelper.getItemStack("minecraft:sand:1"));
		removeAlloy(RecipeHelper.getItemStack("draconicevolution:draconium_ore"), RecipeHelper.getItemStack("minecraft:sand"));
		removeAlloy(RecipeHelper.getItemStack("draconicevolution:draconium_ore"), RecipeHelper.getItemStack("minecraft:sand:1"));
	}

	private void addFluid(int energy, ItemStack input, ItemStack output, FluidStack fluid) {
		try {
			TransposerManager.addFillRecipe(energy, input, output, fluid, false);
		} catch (Exception ex) {
			System.err.println(ex);
		}
	}

	private void addAlloy(int energy, ItemStack primaryInput, ItemStack secondaryInput, ItemStack primaryOutput, ItemStack secondaryOutput, int secondaryChance) {
		try {
			SmelterManager.addRecipe(energy, primaryInput, secondaryInput, primaryOutput, secondaryOutput,
				secondaryChance);
		} catch (Exception ex) {
			System.err.println(ex);
		}
	}

	private void removeAlloy(ItemStack primaryInput, ItemStack secondaryInput) {
		try {
			SmelterManager.removeRecipe(primaryInput, secondaryInput);
		} catch (Exception ex) {
			System.err.println(ex);
		}
	}

}
